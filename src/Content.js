import React, { Component } from 'react';


export class Content extends Component{
    render() {
        return (
            <React.Fragment>
                <table className="table table-stripped">
                    <thead>
                        <tr>
                            <th>Откуда</th>
                            <th>Куда</th>
                            <th>Номер рейса</th>
                            <th>Цена</th>
                            <th>Дата</th>
                            <th>Купить</th>
                        </tr>
                    </thead>
                    <tbody id="table_inner">

                    </tbody>
                </table>
            </React.Fragment>
        );
    }
}




function Flight(props) {
    return React.createElement('tr', null,
        React.createElement('td', null, props.from),
        React.createElement('td', null, props.to),
        React.createElement('td', null, props.number),
        React.createElement('td', null, props.price),
        React.createElement('td', null, props.date),
        React.createElement('td', null,
            React.createElement('a', {href: '/buy', className: "buy_button"}, 'КУПИТЬ')
            ),
    );
}

export const Flights = (
    <React.Fragment>
        <Flight from="Bishkek" to="Moscow" number="582" date="12.03.2019" price="256$" />
        <Flight from="Omsk" to="Tomsk" number="147" date="12.03.2019" price="832$" />
        <Flight from="Astana" to="Moscow" number="963" date="12.03.2019" price="481$" />
        <Flight from="Bishkek" to="Osh" number="654" date="12.03.2019" price="369$" />
        <Flight from="Ekaterinburg" to="Pekin" number="159" date="12.03.2019" price="685$" />
        <Flight from="Bishkek" to="New-York" number="753" date="12.03.2019" price="933$" />
    </React.Fragment>
);