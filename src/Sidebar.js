import React, { Component } from 'react';


export class Sidebar extends Component{
    render() {
        return (
            <React.Fragment>
                <h3>Авиакомпании</h3>
                <ul className="list-unstyled">
                    <li><a href="#">Авиатраффик</a></li>
                    <li><a href="#">Аэрофлот</a></li>
                    <li><a href="#">Люфтганза</a></li>
                    <li><a href="#">НигерияЛайн</a></li>
                    <li><a href="#">СпешлЭйр</a></li>
                    <li><a href="#">Победа</a></li>
                    <li><a href="#">ЛоукостерЭйр</a></li>
                </ul>
            </React.Fragment>
        );
    }
}