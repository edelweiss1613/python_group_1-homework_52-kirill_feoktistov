import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Header} from './Header';
import {Menu} from './Header';
import {Sidebar} from './Sidebar';
import {Content, Flights} from './Content';
import {Footer} from './Footer';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Header />, document.getElementById('header'));
ReactDOM.render(<Menu />, document.getElementById('menu'));
ReactDOM.render(<Sidebar />, document.getElementById('sidebar'));
ReactDOM.render(<Content />, document.getElementById('content'));
ReactDOM.render(Flights, document.getElementById('table_inner'));
ReactDOM.render(<Footer />, document.getElementById('info'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
