import React, { Component } from 'react';

export class Header extends Component {
    render() {
        return (
            <nav className="navbar navbar-default" role="navigation">
                <div className="container">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                    </div>

                    <div className="collapse navbar-collapse" id="menu">
                    </div>
                </div>
            </nav>
        );
    }
}

export class Menu extends Component {
    render() {
        return (
            <ul className="nav navbar-nav">
                <li className="active"><a href="#">Главная</a></li>
                <li><a href="#">Выгодное предложение</a></li>
                <li><a href="#">Избранное</a></li>
                <li><a href="#">Скидки</a></li>
            </ul>
        );
    }
}


